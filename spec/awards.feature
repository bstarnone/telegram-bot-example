@awards
Feature: awards
  
  Scenario: a1 - Command asking for Titanic awards
    Given "/awards Titanic" is sent
    Then the movie "Titanic" has won "11 Oscars"
    And the movie "Titanic" has "128 wins"
    And the movie "Titanic" has "83 nominations total"

  Scenario: a2 - Command asking for Titanic and Casablanca awards
    Given "/awards Titanic,Casablanca" is sent
    Then the movie "Titanic" has won "11 Oscars"
    And the movie "Titanic" has "128 wins"
    And the movie "Titanic" has "83 nominations total"
    And the movie "Casablanca" has won "3 Oscars"
    And the movie "Casablanca" has "14 wins"
    And the movie "Casablanca" has "11 nominations total"

  Scenario: a3 - Command asking for a movie without awards
    Given "/awards terminator" is sent
    Then the movie "terminator" has not won anything

  Scenario: a4 - Command asking for non existant movie
    Given "/awards notamovie" is sent
    Then there is a message saying it could not be found