require 'spec_helper'
require 'web_mock'

describe 'MovieAwards' do
  titanic_awards = 'Won 11 Oscars. 126 wins & 83 nominations total'
  casablanca_awards = 'Won 3 Oscars. 14 wins & 11 nominations total'

  it 'should return the awards for Titanic' do
    stub_omdb_titanic
    awards = MovieAwards.new('Titanic').awards
    expect(awards).to eq(titanic_awards)
  end

  it 'should return the awards for Titanic and Casablanca' do
    stub_endpoints
    awards = []
    awards << MovieAwards.new('Titanic').awards
    awards << MovieAwards.new('casablanca').awards
    expect(awards).to eq([titanic_awards, casablanca_awards])
  end

  it 'should return N/A for the awards of Terminator' do
    stub_omdb_terminator
    awards = MovieAwards.new('terminator').awards
    expect(awards).to eq('N/A')
  end

  it 'should return that it is not found when movie is not a movie' do
    stub_omdb_not_a_movie
    awards = MovieAwards.new('notamovie').awards
    expect(awards).to eq('Movie not found!')
  end
end
