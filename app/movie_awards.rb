class MovieAwards
  attr_accessor :awards

  OMDB_API_URL = 'https://www.omdbapi.com/'.freeze
  def initialize(movie_name)
    @movie_name = movie_name
    @awards = get_awards_for_movie(movie_name)
  end

  def get_awards_for_movie(movie_name)
    response = Faraday.get(OMDB_API_URL, { t: movie_name, apikey: ENV['OMDB_API_KEY'] })
    response_json = JSON.parse(response.body)
    return response_json['Error'] if response_json['Response'] == 'False'

    response_json['Awards']
  end
end
